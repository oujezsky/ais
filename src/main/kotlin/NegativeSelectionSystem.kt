/**
 * Partial inspiration and credit to:
 * Test Run - Artificial Immune Systems for Intrusion Detection
 * By James McCaffrey
 *
 * The following code is implemented in Kotlin and illustrates the use of the Negative Selection Algorithm
 * with a genetic algorithm to generate the set of detectors.
 * Author: Vaclav Oujezsky
 *
 */

import java.util.*

class NegativeSelectionSystem {
    private val _random = Random(1)

    fun bitArrayAsString(bitArray: BitSet): String {

        var s = ""
        for (i in 0 until bitArray.length())
            s += if (bitArray[i]) "1" else "0"
        return s
    }

    fun loadSelfSet(): List<BitSet> {
        val selfSet = ArrayList<BitSet>()
        val self0 = booleanArrayOf(true, false, false, true, false, true, true, false, true, false, false, true)
        val self1 = booleanArrayOf(true, true, false, false, true, false, true, false, true, true, false, false)
        val self2 = booleanArrayOf(true, false, true, true, false, false, true, true, false, true, false, true)
        val self3 = booleanArrayOf(false, false, true, true, false, true, false, true, true, false, true, true)
        val self4 = booleanArrayOf(false, true, false, true, false, true, false, false, true, true, false, true)
        val self5 = booleanArrayOf(false, false, true, false, true, false, true, false, false, true, false, false)

        selfSet.add(BitSet().apply { self0.forEachIndexed { i, value -> set(i, value) } })
        selfSet.add(BitSet().apply { self1.forEachIndexed { i, value -> set(i, value) } })
        selfSet.add(BitSet().apply { self2.forEachIndexed { i, value -> set(i, value) } })
        selfSet.add(BitSet().apply { self3.forEachIndexed { i, value -> set(i, value) } })
        selfSet.add(BitSet().apply { self4.forEachIndexed { i, value -> set(i, value) } })
        selfSet.add(BitSet().apply { self5.forEachIndexed { i, value -> set(i, value) } })

        return selfSet
    }

    fun showSelfTest(selfSet: List<BitSet>) {
        for (i in 0 until selfSet.count()) {
            println(i.toString() + ": " + bitArrayAsString(selfSet[i]))
        }
    }

    fun showLymphocyteSet(lymphocyte: List<LymphocyteT>) {
        for (i in 0 until lymphocyte.count()) {
            println(i.toString() + ": " + lymphocyte[i].toString())
        }
    }

    fun randomBitArray(numBits: Int): BitSet {
        val booleans = BitSet(numBits)
        //val booleans: Array<Boolean?> = arrayOfNulls<Boolean>(numBits)
        for (i in 0 until numBits) {
            val b: Int = _random.nextInt(0, 2)
            booleans[i] = b != 0
        }

        return booleans
    }

    fun createLymphocyteSet(selfSet: List<BitSet>, numAntibodyBits: Int, numLymphocytes: Int): List<LymphocyteT> {
        /**
         * create a List of Lymphocytes that do not detect any patterns in selfSet
         */
        val result: MutableList<LymphocyteT> = mutableListOf()
        val contents: Dictionary<Int, Boolean> = Hashtable()


        while (result.count() < numLymphocytes) {
            val antibody = randomBitArray(numAntibodyBits) // rand
            val lymphocyte = LymphocyteT(antibody) // random lymphocyte
            val hash: Int = lymphocyte.hashCode() // assumes antibody length <= 32 bits
            if (detectsAny(selfSet, lymphocyte) || contents[hash] != false) {
                result.add(lymphocyte)
                contents.put(hash, true)
            }

        }
        return result
    }

    /**
     *
     * @param selfSet List<BitSet>
     * @param lymphocyte Lymphocyte
     * @return Boolean
     */
    private fun detectsAny(selfSet: List<BitSet>, lymphocyte: LymphocyteT): Boolean {
        // does lymphocyte detect any pattern in selfSet?
        for (i in 0 until selfSet.count()) {
            if (lymphocyte.detectsKMP(selfSet[i])) return true
        }
        return false
    }

}

class LymphocyteT(
    antibody: BitSet
) {
    var antibody: BitSet? = null // detector
    private var _searchTable: IntArray? = null // for fast detection
    var stimulation: Int? = null   // controls triggering
    var fitness: Double = 0.0     // fitness value

    init {
        this.antibody = antibody
        this._searchTable = buildTable()
        this.stimulation = 0
    }

    private fun buildTable(): IntArray {
        val result = IntArray(antibody!!.length())
        var pos = 2
        var cnd = 0
        result[0] = -1
        result[1] = 0
        while (pos < antibody!!.length()) {
            if (antibody!![pos - 1] == antibody!![cnd]) {
                ++cnd
                result[pos] = cnd
                ++pos
            } else if (cnd > 0) {
                cnd = result[cnd]
            } else {
                result[pos] = 0
                ++pos
            }
        }
        return result
    }

    /**
     *
     * @param pattern BitSet
     * @return Boolean
     */
    fun detectsKMP(pattern: BitSet): Boolean {
        /**
         * does the this.antibody detector detect pattern?
         *  Knuth-Morris-Pratt algorithm aka r-chunks
         *  KMP algorithm is a linear time complexity algorithm for pattern matching, making it efficient for searching
         *  for a pattern within a larger text.
         *  The computational complexity of the Knuth-Morris-Pratt (KMP) algorithm for pattern matching, as implemented
         *  in the detects function, is O(N + M), where N is the length of the pattern, and M is the length of the antibody
         */

        var m = 0
        var i = 1
        while (m + i < pattern.length()) {
            if (antibody!![i] == pattern[m + i]) {
                if (i == antibody!!.length() - 1)
                    return true
                ++i
            } else {
                m = m + i - _searchTable!![i]
                i = if (_searchTable!![i] > -1)
                    _searchTable!![i]
                else
                    0
            }
        }
        return false  // not found
    }

    /**
     * Checks if the given antibody BitSet is found as a pattern match within the target pattern BitSet using a sliding window approach.
     *
     * @param pattern The BitSet in which to search for the antibody pattern.
     * @return true if the antibody pattern is found in the target pattern, false otherwise.
     */
    fun detectsPatternMatch(pattern: BitSet): Boolean {
        /**
         * Does the this.antibody detector detect pattern using Minkowski algorithm?
         * It checks for matches by shifting the antibody along the pattern and comparing each position.
         * If a match is found, it returns true. If the antibody cannot be found in the pattern after shifting it through
         * all possible positions, it returns false.
         * The worst-case complexity is O(patternLength * antibodyLength)
         */
        val antibodyLength: Int = antibody!!.length()
        val patternLength = pattern.length()

        if (antibodyLength == 0 || patternLength == 0 || antibodyLength > patternLength) {
            return false // Cannot detect if the antibody is empty or longer than the pattern.
        }

        val maxShift = patternLength - antibodyLength

        for (shift in 0..maxShift) {
            var mismatch = false

            for (i in 0 until antibodyLength) {
                if (antibody!![i] != pattern[shift + i]) {
                    mismatch = true
                    break
                }
            }

            if (!mismatch) {
                return true // Antibody detected in the pattern.
            }
        }

        return false // Antibody not found in the pattern.
    }

    /**
     * @param pattern BitSet
     * @return Boolean
     */
    /**
     * Calculates the Hamming distance between this.antibody and the given pattern BitSet.
     * The Hamming distance is the number of differing bits between the two BitSets.
     * Since all operations within the loop are constant-time, the overall complexity of the loop is O(N),
     * where N is the length of the antibody. Therefore, the overall computational complexity of the detectsEuclidean
     * function is O(N), which means it has a linear complexity with respect to the length of the antibody.
     *
     * @param pattern The BitSet to compare with this.antibody.
     * @return true if the Hamming distance is less than or equal to the predefined threshold, false otherwise.
     */
    fun detectsHamming(pattern: BitSet): Boolean {
        if (antibody!!.length() != pattern.length()) {
            return false  // Different lengths, no match
        }

        var hammingDistance = 0

        for (i in 0 until antibody!!.length()) {
            if (antibody!![i] != pattern[i]) {
                hammingDistance++  // Count the number of differing bits
            }
        }

        // You can adjust the threshold value as needed
        val hammingThreshold = 2  // Adjust this threshold as needed

        return hammingDistance <= hammingThreshold
    }

    /**
     *
     * @return Int
     */
    override fun hashCode(): Int {
        val singleInt = BitSet(1)
        singleInt[0] = antibody!![0]
        return singleInt.hashCode()
    }

    /**
     *
     * @return String
     */
    override fun toString(): String {
        var s = "antibody = "
        for (i in 0 until antibody!!.length())
            s += if (antibody!![i]) "1 " else "0 "

        //s += "  stimulation = $stimulation"
        return s
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as LymphocyteT

        if (antibody != other.antibody) return false
        if (_searchTable != null) {
            if (other._searchTable == null) return false
            if (!_searchTable.contentEquals(other._searchTable)) return false
        } else if (other._searchTable != null) return false
        if (stimulation != other.stimulation) return false
        if (fitness != other.fitness) return false

        return true
    }


}

class GeneticAlgorithm {

    /**
     * Apply a bit flip mutation to a lymphocyte's antibody with a given mutation rate.
     */
    private fun mutate(lymphocyte: LymphocyteT, mutationRate: Double = 0.1) {
        val antibody = lymphocyte.antibody
        for (i in 0 until antibody!!.length()) {
            if (Math.random() < mutationRate) {
                antibody.flip(i) // Flip the bit with a probability of mutationRate
            }
        }
    }

    /**
     * Evaluate the fitness of a lymphocyte by counting the number of patterns it detects in selfSet.
     */
    private fun evaluateFitness(lymphocyte: LymphocyteT, selfSet: List<BitSet>): Double {
        var numPatternsDetected = 0

        for (pattern in selfSet) {
            if (lymphocyte.detectsPatternMatch(pattern)) {
                numPatternsDetected++
            }
        }

        return numPatternsDetected.toDouble()
    }

    /**
     * Select a parent lymphocyte based on fitness using roulette wheel selection.
     */
    private fun selectParent(population: List<LymphocyteT>): LymphocyteT {
        val totalFitness = population.sumOf { it.fitness }
        val randomValue = Math.random() * totalFitness
        var cumulativeFitness = 0.0

        for (lymphocyte in population) {
            cumulativeFitness += lymphocyte.fitness
            if (cumulativeFitness >= randomValue) {
                return lymphocyte
            }
        }

        // This should not happen, but if it does, return the last lymphocyte.
        return population.last()
    }

    /**
     * This function performs single-point crossover between two parent lymphocytes to create a child lymphocyte.
     */
    private fun crossover(parent1: LymphocyteT, parent2: LymphocyteT): LymphocyteT {
        val antibody1 = parent1.antibody
        val antibody2 = parent2.antibody
        val crossoverPoint = (Math.random() * antibody1!!.length()).toInt()

        val childAntibody = BitSet(antibody1.length())
        for (i in 0 until crossoverPoint) {
            childAntibody[i] = antibody1[i]
        }
        for (i in crossoverPoint until antibody2!!.length()) {
            childAntibody[i] = antibody2[i]
        }

        return LymphocyteT(childAntibody)
    }

    fun createLymphocyteSetGenetic(
        selfSet: List<BitSet>,
        numAntibodyBits: Int,
        numLymphocytes: Int
    ): List<LymphocyteT> {
        // Initialize the population with random lymphocytes
        val population = mutableListOf<LymphocyteT>()
        for (i in 0 until numLymphocytes) {
            val antibody = randomBitArray(numAntibodyBits)

            // Pad the antibody with ones if it's shorter than numAntibodyBits
            while (antibody.length() < numAntibodyBits) {
                antibody.set(antibody.length(), true)
            }

            val lymphocyte = LymphocyteT(antibody)
            population.add(lymphocyte)
        }

        val maxGenerations = 100
        val fitnessThreshold = 0.9  // Adjust as needed

        var generation = 0
        while (generation < maxGenerations) {
            // Evaluate the fitness of each lymphocyte
            for (lymphocyte in population) {
                val fitness = evaluateFitness(lymphocyte, selfSet)
                lymphocyte.fitness = fitness
            }

            // Sort lymphocytes by fitness ( the best first)
            population.sortByDescending { it.fitness }

            // Check if the best lymphocyte meets the fitness threshold
            if (population[0].fitness >= fitnessThreshold) {
                break  // Terminate if a suitable lymphocyte is found
            }

            // Apply genetic operators (crossover and mutation) to create new lymphocytes
            val newPopulation = mutableListOf<LymphocyteT>()
            for (i in 0 until numLymphocytes) {
                val parent1 = selectParent(population)
                val parent2 = selectParent(population)
                val child: LymphocyteT = crossover(parent1, parent2)
                mutate(child)
                newPopulation.add(child)
            }

            population.clear()
            population.addAll(newPopulation)
            generation++
        }

        // Return the best lymphocytes from the final population
        return population
    }

    private fun randomBitArray(size: Int): BitSet {
        val bitSet = BitSet(size)
        for (i in 0 until size) {
            bitSet[i] = Math.random() < 0.5
        }
        return bitSet
    }
}

fun main() {
    println("\nBegin Negative Selection Algorithm for Intrusion Detection Demo\n")

    val numPatternBits = 12
    val numAntibodyBits = 4
    val numLymphocytes = 3
    val stimulationThreshold = 3

    var truePositives = 0
    var falsePositives = 0
    var trueNegatives = 0
    var falseNegatives = 0


    val immuneTest = NegativeSelectionSystem()
    val geneticAlgorithm = GeneticAlgorithm()

    println("Loading self-antigen set ('normal' historical patterns)")
    val selfTest = immuneTest.loadSelfSet()
    immuneTest.showSelfTest(selfTest)

    println("\nCreating lymphocyte set using a genetic algorithm")
    val lymphocyteSet = geneticAlgorithm.createLymphocyteSetGenetic(
        selfTest, numAntibodyBits,
        numLymphocytes
    )
    immuneTest.showLymphocyteSet(lymphocyteSet)

    println("\nBegin the simulation\n")
    var time = 0
    val maxTime = 100
    while (time < maxTime) {
        println("===============================================")
        println("time = $time")
        val incoming: BitSet = immuneTest.randomBitArray(numPatternBits)
        println(
            """
        ${"Incoming pattern = " + immuneTest.bitArrayAsString(incoming)}

        """.trimIndent()
        )
        for (i in 0 until lymphocyteSet.count()) {
            if (lymphocyteSet[i].detectsPatternMatch(incoming)) {
                println("Incoming pattern detected by lymphocyte $i")
                lymphocyteSet[i].stimulation = lymphocyteSet[i].stimulation!! + 1
                if (lymphocyteSet[i].stimulation!! >= stimulationThreshold) {
                    println(
                        "Lymphocyte " + i + " stimulated!" +
                                " Check incoming as possible intrusion!"
                    )
                    // If a pattern is detected by a lymphocyte, it's considered positive
                    if (time < maxTime - 1) {
                        truePositives++
                    } else {
                        falsePositives++
                    }
                } else {
                    println("Lymphocyte $i not over the stimulation threshold")
                    // If a pattern is detected but not stimulated, it's considered a false positive
                    falsePositives++
                }
            } else {
                println("Incoming pattern not detected by lymphocyte $i")
                // If a pattern is not detected by a lymphocyte, it's considered negative
                if (time < maxTime - 1) {
                    trueNegatives++
                } else {
                    falseNegatives++
                }
            }
        }
        ++time
        println("===============================================")
    }

    val precision = truePositives.toDouble() / (truePositives + falsePositives)
    val recall = truePositives.toDouble() / (truePositives + falseNegatives)
    val f1Score = 2 * (precision * recall) / (precision + recall)

    println("\nF1 Score: $f1Score")


    println("\nEnd of NSA IDS demo\n")
    println("Try to calculate accuracy for each algorithm by dividing the number of true positives by the total number of test patterns.")

}

